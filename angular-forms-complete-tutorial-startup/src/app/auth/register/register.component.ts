import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
eventForm:FormGroup;
  constructor(private _fb:FormBuilder, private _auth:AuthService,private _router:Router) { }

  ngOnInit(): void {
        this.eventForm = this._fb.group({
      email:[null ,Validators.required],
      password:[null,Validators.required],

    });
  }

  registerUser(){
    if(this.eventForm.invalid){
      console.log(this.eventForm);
Object.keys(this.eventForm.controls).forEach(key => {
  this.eventForm.controls[key].markAsTouched();
});
      return ;
      }
    this._auth.register(this.eventForm.value).subscribe((response)=>{
      console.log(response['token']);
       localStorage.setItem('token', response.token)
       this._router.navigate(['/special'])
    }, error=>{
      console.log('error');
    })

  }

onSubmit(){
 console.log(this.eventForm.value);
}

}


