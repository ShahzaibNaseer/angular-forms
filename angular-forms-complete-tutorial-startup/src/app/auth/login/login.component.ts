import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm:FormGroup;
  constructor( private _fb:FormBuilder, private _auth:AuthService,private _router:Router) { }

  ngOnInit(): void {
           this.loginForm = this._fb.group({
      email:[null ,Validators.required],
      password:[null,Validators.required],

    });
  }

  loginUser(){
    if(this.loginForm.invalid){
      console.log(this.loginForm);
Object.keys(this.loginForm.controls).forEach(key => {
  this.loginForm.controls[key].markAsTouched();
});
      return ;
      }
    this._auth.login(this.loginForm.value).subscribe((response)=>{
      localStorage.setItem ('token', response.token);
      console.log(response.token );
      console.log(this._auth.getToken());
      if(this._auth.getToken()===response.token){
       this._router.navigate(['/special'])
      }

    },error=>{
console.log(error)
    })

  }

  dada(){
    alert('somhbj');
  }

}
