import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

private url= environment.authUrl;

  constructor(private http:HttpClient, private _router :Router) { }

  register(user){
   return this.http.post<any>(`http://${this.url}/api/register`,user)
  }

  login(user){
    return this.http.post<any>(`http://${this.url}/api/login`,user)
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }
  getToken(){
    return localStorage.getItem('token')
  }

  logoutUser(){
   localStorage.removeItem('token');
    this._router.navigate(['/events'])

  }



}
