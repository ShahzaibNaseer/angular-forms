import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http"
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EventService {

  private eventsUrl= environment.authUrl;
  constructor(private http:HttpClient) {  }

  getEvents(){
    return this.http.get<any>(`http://${this.eventsUrl}/api/events`)
  }

  getSpecialEvents(){
      return this.http.get<any>(`http://${this.eventsUrl}/api/special`)
  }


}
