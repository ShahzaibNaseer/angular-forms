import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EventService } from '../eventService/event.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent implements OnInit {
specialEvents :any=[]
  constructor(private service:EventService, private _router: Router) { }

  ngOnInit(): void {
    this.getSpecialEvents();
  }

  getSpecialEvents(){
    this.service.getSpecialEvents().subscribe( response=>{
      this.specialEvents =response;
    },error=>{
      if (error instanceof HttpErrorResponse){
        if (error.status==500){
          this._router.navigate(['/login'])
        }

      }
    })
  }

}
