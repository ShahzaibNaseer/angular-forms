import { Component, OnInit } from '@angular/core';
import { EventService } from '../eventService/event.service';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
 events: any =[];
  constructor(private service:EventService ) { }

  ngOnInit(): void {
    this.getEvents();
  }
getEvents(){
  this.service.getEvents().subscribe((response)=>{
    this.events=response;
  }, error=>{
    console.log(error);
  })
}


}
