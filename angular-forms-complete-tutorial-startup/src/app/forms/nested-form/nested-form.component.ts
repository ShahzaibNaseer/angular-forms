import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-nested-form',
  templateUrl: './nested-form.component.html',
  styleUrls: ['./nested-form.component.css']
})
export class NestedFormComponent implements OnInit {

  states: Array<String> = ['AR', 'AL', 'CA', 'DC'];
  fruits: Array<String> = ['Mango', 'Grapes', 'Strawberry', 'Oranges'];
  selectedFruits=[];
  fruitsErrors:Boolean=true;

  nestedForm:FormGroup;
  constructor( private _fb:FormBuilder) { }

  ngOnInit() {
    this.nestedForm= this._fb.group({
      firstName:[],
      lastName:[],
      favFruits:this.addFruitsControls(),
      address:this._fb.array([this.addressGroup()]),



    })
  }

  addFruitsControls(){
    const arr= this.fruits.map((elements)=>{
      return this._fb.control(false);
    });
    return this._fb.array(arr)
  }

checkFruitControlsTouched(){
  let flg=false;
  this.fruitsArray.controls.forEach((control,i)=>{
   if(control.touched){
     flg=true;
   }
 });
return flg;
}

  getSelectedFruitsValue(){
    this.selectedFruits=[];
 this.fruitsArray.controls.forEach((control,i)=>{
   if(control.value){
     this.selectedFruits.push(this.fruits[i])
   }
 });
 this.fruitsErrors=this.selectedFruits.length>0? false:true;
 console.log(this.selectedFruits);
  }



  addressGroup(){
    return this._fb.group({
        primaryFlg:[],
        streetAddress:[],
        city:[],
        state:[],
        zipcode:[]

    })
  }

  get addressArray(){
    return <FormArray> this.nestedForm.get('address');
  }

   get fruitsArray(){
    return <FormArray> this.nestedForm.get('favFruits');
  }
  addAddress(){
    this.addressArray.push(this.addressGroup())
  }

  removeAddress(index){
    this.addressArray.removeAt(index);
  }

  submitHandler(){
    console.log(this.nestedForm) ;
  }

}
