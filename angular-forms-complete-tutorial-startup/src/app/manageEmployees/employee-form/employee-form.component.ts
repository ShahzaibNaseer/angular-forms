import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {EmployeeService} from '../../services/employee.service'

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
empolyees:any=[];
employeeForm:FormGroup;
pageTitle = 'Create Employee';
formMode = 'create';
updated:boolean=false;
uniqueId:any;
employeeId: string;

  constructor( private _fb: FormBuilder,private service:EmployeeService,private router:ActivatedRoute,private toastr: ToastrService ) { }

  ngOnInit(): void {
    this.employeeForm = this._fb.group({
      name:[null ,Validators.required],
      position:[null,Validators.required],
      office:[null,Validators.required],
      salary:[null, [Validators.required,Validators.pattern("^[0-9]*$")]],
    });
    this.getAllEmployees();
this.toastr.success('All Data Receieved');
  }

getAllEmployees():void {
  this.service.getPost().subscribe(response=>{
      this.empolyees = response;
      console.log(this.empolyees);
    })
}
  
 
  createPost(){
    console.log('form', this.employeeForm.value);
   this.service.createPost(this.employeeForm.value) .subscribe(response=>{
   this.employeeForm.reset();
   this.getAllEmployees();
      console.log(response);
    },
    (error)=> {});
  }

deletePost(id){
  this.service.deletePost(id).subscribe(response=>{
       this.getAllEmployees();
       this.toastr.warning('Deleted')
  });  
}

scrollToTop(el) {
    el.scrollTop = 0;     
}

onEdit(id,el){
  console.log(id);
  this.employeeId = id;
 this.service.uniquePost(id).subscribe((response:any)=>{
   this.employeeForm.setValue({
      name:response.name,
      position:response.position,
      office:response.office,
      salary:response.salary
    })
    this.formMode='updated';
    this.pageTitle = 'Update Employee';
     this.toastr.success('Fields Populated');
      
})
 
}
  // showSuccess() {
  //   this.toastr.success('Hello world!', 'Toastr fun!');
  // }


updatePost(id){
  console.log(id);
this.service.updatePost(id,this.employeeForm.value).subscribe(response=>{
       this.getAllEmployees();
  })
}


  onSubmitHandler(){
    if(this.employeeForm.invalid) {
      return;
    }
    if(this.formMode === 'create') {
 this.service.createPost(this.employeeForm.value) .subscribe(response=>{
   this.employeeForm.reset();
   this.getAllEmployees();
      console.log(response);
     this.toastr.success('Employee Created');

    },
    (error)=> {});
    } else {
      console.log('form', this.employeeForm.value)
      this.service.updatePost(this.employeeId,this.employeeForm.value).subscribe(response=>{
       this.getAllEmployees();
       this.employeeForm.reset();
       this.formMode = 'create';
       this.pageTitle = 'Create Employee';
     this.toastr.show('Employee Updated');
     
     

       
  })
    }
    
  }




}
