import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
private url='http://appcrates.net:3500/employees';

  constructor( private http:HttpClient) { }

  getPost(){
    return this.http.get(this.url);
  }

  createPost(employeeForm){
return this.http.post(this.url,employeeForm)
  }

  deletePost(id){
    return this.http.delete(`${this.url}/${id}`);
  }

  uniquePost(id){
    return this.http.get(`${this.url}/${id}`)
  }

  updatePost(id,employeeForm){
     return  this.http.put(`${this.url}/${id}`,employeeForm)
  }
}
