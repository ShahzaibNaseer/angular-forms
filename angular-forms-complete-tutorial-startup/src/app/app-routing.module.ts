import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateSampleComponent } from './forms/template-sample/template-sample.component';
import { ReactiveSampleComponent } from './forms/reactive-sample/reactive-sample.component';
import { NestedFormComponent } from './forms/nested-form/nested-form.component';
import { EmployeeFormComponent } from './manageEmployees/employee-form/employee-form.component';
import { EventsComponent } from './auth/events/events.component';
import { SpecialEventsComponent } from './auth/special-events/special-events.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/authGuard/auth.guard';
// redirectTo: 'template', pathMatch: 'full'
const routes: Routes = [
  { path: '',  redirectTo: 'events', pathMatch: 'full' },
  {path: 'events', component: EventsComponent },
  {path: 'special', component: SpecialEventsComponent,canActivate:[AuthGuard]},
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'employee', component: EmployeeFormComponent},
  {path: 'employee/:id', component: EmployeeFormComponent},
  // { path: 'template', component: TemplateSampleComponent },
  // { path: 'reactive', component: ReactiveSampleComponent },
  { path: 'nested', component: NestedFormComponent ,canActivate:[AuthGuard]},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
